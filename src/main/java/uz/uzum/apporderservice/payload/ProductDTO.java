package uz.uzum.apporderservice.payload;

import lombok.Data;

@Data
public class ProductDTO {

    private String id;

    private String name;

    private Double price;
}
