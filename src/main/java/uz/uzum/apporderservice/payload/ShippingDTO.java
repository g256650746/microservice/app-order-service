package uz.uzum.apporderservice.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShippingDTO {

    private UUID id;

    private String address;

    private UUID orderId;

    private LocalDateTime createdAt;
}
