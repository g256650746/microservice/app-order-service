package uz.uzum.apporderservice.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@EqualsAndHashCode(exclude = {"quantity"})
public class OrderItemDTO {

    private String productId;

    private Integer quantity;
}
