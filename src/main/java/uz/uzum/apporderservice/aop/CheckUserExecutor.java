package uz.uzum.apporderservice.aop;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import uz.uzum.apporderservice.service.CacheService;
import uz.uzum.apporderservice.utils.AppConstants;

import java.util.Arrays;
import java.util.Set;

import static uz.uzum.apporderservice.aop.AppRequest.CURRENT_USER_ATTRIBUTE;
import static uz.uzum.apporderservice.aop.AppRequest.currentRequest;

@Aspect
@Component
@RequiredArgsConstructor
public class CheckUserExecutor {

    private final CacheService cacheService;

    @Before(value = "@annotation(checkUser)")
    public void checkAuthExecutor(CheckUser checkUser) {
        check(checkUser);
    }

    private void check(CheckUser checkUser) {
        HttpServletRequest request = currentRequest();

        String token = request.getHeader("Authorization");

        if (token == null)
            throw new RuntimeException();
        try {
            UserDTO userDTO = cacheService.getUserFromAuthService(token);
            if (!hasPermissions(checkUser.permissions(), userDTO.getPermissions()))
                throw new RuntimeException();
            else {
                System.out.println("Hammasi ok: " + userDTO);
                request.setAttribute(CURRENT_USER_ATTRIBUTE, userDTO);
            }
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean hasPermissions(String[] expected, Set<String> actual) {
//        for (String permission : expected)
//            if (actual.contains(permission))
//                return true;
//
//        return false;
        return Arrays.stream(expected).anyMatch(actual::contains);
    }
}
