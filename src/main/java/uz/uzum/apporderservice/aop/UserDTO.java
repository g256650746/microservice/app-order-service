package uz.uzum.apporderservice.aop;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Data
public class UserDTO implements Serializable {

    private UUID id;

    private String name;

    private Set<String> permissions;
}
