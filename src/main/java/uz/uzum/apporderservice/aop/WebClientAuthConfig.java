package uz.uzum.apporderservice.aop;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancedExchangeFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;
import uz.uzum.apporderservice.aop.AuthClient;
import uz.uzum.apporderservice.client.ShippingClient;

@Configuration
@RequiredArgsConstructor
public class WebClientAuthConfig {


    private final LoadBalancedExchangeFilterFunction filterFunction;



    @Bean
    public WebClient authWebClient() {
        return WebClient.builder()
                .baseUrl("http://auth-service")
                .filter(filterFunction)
                .build();
    }

    @Bean
    public AuthClient authClient() {
        HttpServiceProxyFactory httpServiceProxyFactory
                = HttpServiceProxyFactory
                .builder(WebClientAdapter.forClient(authWebClient()))
                .build();
        return httpServiceProxyFactory.createClient(AuthClient.class);
    }
}
