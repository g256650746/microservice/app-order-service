package uz.uzum.apporderservice.aop;

import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

@HttpExchange
public interface AuthClient {

    @GetExchange("/api/auth/user/validate-token")
    UserDTO checkUserByToken(@RequestHeader(value = "Authorization") String token);
}
