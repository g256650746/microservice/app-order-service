package uz.uzum.apporderservice.aop;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class AppRequest {


    public static HttpServletRequest currentRequest() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes == null)
            throw new RuntimeException();
        return servletRequestAttributes.getRequest();
//        return Optional.ofNullable(servletRequestAttributes)
//                .map(ServletRequestAttributes::getRequest).orElse(null);
    }

    public static final String CURRENT_USER_ATTRIBUTE = "user";

}
