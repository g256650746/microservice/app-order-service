package uz.uzum.apporderservice.utils;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public interface AppConstants {

    String BASE_PATH = "/api/order";

}
