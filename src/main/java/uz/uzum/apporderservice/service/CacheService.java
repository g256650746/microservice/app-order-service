package uz.uzum.apporderservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import uz.uzum.apporderservice.aop.AuthClient;
import uz.uzum.apporderservice.aop.UserDTO;

@Service
@RequiredArgsConstructor
public class CacheService {

    private final AuthClient authClient;


    @Cacheable(value = "users",key = "#token")
    public UserDTO getUserFromAuthService(String token) {
        return authClient.checkUserByToken(token);
    }
}
