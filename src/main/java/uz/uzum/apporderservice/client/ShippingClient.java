package uz.uzum.apporderservice.client;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import uz.uzum.apporderservice.payload.ShippingDTO;

@HttpExchange
public interface ShippingClient {


    @PostExchange("/api/shipping/shipping")
    ShippingDTO createShipping(@RequestBody ShippingDTO shippingDTO);
}
