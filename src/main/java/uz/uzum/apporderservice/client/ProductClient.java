package uz.uzum.apporderservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import uz.uzum.apporderservice.payload.ProductDTO;

import java.util.List;

@FeignClient(value = "product-service")
public interface ProductClient {

    @GetMapping("/api/product/product/{id}")
    ProductDTO getProductById(@PathVariable Integer id);

    @PostMapping("/api/product/product/by-ids")
    List<ProductDTO> getProductsByIds(@RequestBody List<String> productIds);
}
