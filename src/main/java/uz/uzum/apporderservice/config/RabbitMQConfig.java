package uz.uzum.apporderservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {


    @Value(value = "${rabbit.exchange.uzum}")
    private String uzumExchange;

    @Value(value = "${rabbit.queue.shipping.name}")
    private String queueForShipping;

    @Value(value = "${rabbit.queue.shipping.routingKey}")
    private String routingKeyForShipping;


    @Bean
    public TopicExchange ketmonExchange() {
        return new TopicExchange(uzumExchange);
    }

    @Bean
    public Queue queueForShipping() {
        return new Queue(queueForShipping);
    }

    @Bean
    public Binding binding() {
        return BindingBuilder
                .bind(queueForShipping())
                .to(ketmonExchange())
                .with(routingKeyForShipping);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }
}
