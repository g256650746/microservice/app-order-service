package uz.uzum.apporderservice.config;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancedExchangeFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;
import uz.uzum.apporderservice.aop.AuthClient;
import uz.uzum.apporderservice.client.ShippingClient;

@Configuration
@RequiredArgsConstructor
public class WebClientConfig {


    private final LoadBalancedExchangeFilterFunction filterFunction;

    @Bean
    public WebClient shippingWebClient() {
        return WebClient.builder()
                .baseUrl("http://shipping-service")
                .filter(filterFunction)
                .build();
    }

    @Bean
    public ShippingClient shippingClient() {
        HttpServiceProxyFactory httpServiceProxyFactory
                = HttpServiceProxyFactory
                .builder(WebClientAdapter.forClient(shippingWebClient()))
                .build();
        return httpServiceProxyFactory.createClient(ShippingClient.class);
    }

}
