package uz.uzum.apporderservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import uz.uzum.apporderservice.aop.CheckUser;
import uz.uzum.apporderservice.aop.UserDTO;
import uz.uzum.apporderservice.client.ProductClient;
import uz.uzum.apporderservice.client.ShippingClient;
import uz.uzum.apporderservice.entity.Order;
import uz.uzum.apporderservice.entity.OrderItem;
import uz.uzum.apporderservice.payload.OrderDTO;
import uz.uzum.apporderservice.payload.OrderItemDTO;
import uz.uzum.apporderservice.payload.ProductDTO;
import uz.uzum.apporderservice.payload.ShippingDTO;
import uz.uzum.apporderservice.repository.OrderRepository;
import uz.uzum.apporderservice.utils.AppConstants;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static uz.uzum.apporderservice.aop.AppRequest.CURRENT_USER_ATTRIBUTE;
import static uz.uzum.apporderservice.aop.AppRequest.currentRequest;

@RestController
@RequestMapping(AppConstants.BASE_PATH + "/order")
@RequiredArgsConstructor
public class OrderController {

    private final ProductClient productClient;
    private final OrderRepository orderRepository;
    private final ShippingClient shippingClient;
    private final RabbitTemplate rabbitTemplate;
    private final Environment environment;

    @Value(value = "${rabbit.exchange.uzum}")
    private String uzumExchange;

    @Value(value = "${rabbit.queue.shipping.routingKey}")
    private String routingKeyForShipping;

//    @Value(value = "${app.shipping.key}")
//    private String key;


    //private ()
    @CheckUser
    @Transactional
    @PostMapping
    public Order add(@RequestBody LinkedHashSet<OrderItemDTO> orderItemDTOList) {

        Order order = new Order();

        UserDTO user = (UserDTO) currentRequest().getAttribute(CURRENT_USER_ATTRIBUTE);

        order.setUserId(user.getId());
        List<String> productIds = orderItemDTOList.stream().map(OrderItemDTO::getProductId).toList();

        //product-service products[]
        List<ProductDTO> products = productClient.getProductsByIds(productIds);
        List<OrderItem> orderItems = new LinkedList<>();

        AtomicReference<Double> amount = new AtomicReference<>((double) 0);
        orderItemDTOList.forEach(orderItemDTO -> {
            Double price = products.stream().filter(productDTO ->
                            Objects.equals(productDTO.getId(), orderItemDTO.getProductId()))
                    .findFirst().orElseThrow().getPrice();
            amount.updateAndGet(v -> v + (orderItemDTO.getQuantity() * price));
            OrderItem orderItem = new OrderItem(
                    order,
                    orderItemDTO.getProductId(),
                    orderItemDTO.getQuantity(),
                    price
            );
            orderItems.add(orderItem);
        });

        order.setAmount(amount.get());
        order.setOrderItems(orderItems);

        orderRepository.save(order);
        //request
        ShippingDTO shippingDTO = ShippingDTO.builder()
                .orderId(order.getId())
                .address("Addresjon")
                .build();

        rabbitTemplate.convertAndSend("uzum_service", "faqat-shipping-queuga", order.getId() + "/" + "Bizdan");


//        ShippingDTO shipping = shippingClient.createShipping(shippingDTO);
//        System.out.println(shipping);

        return order;
    }

    @CheckUser(permissions = {"LIST_ORDER"})
    @GetMapping
    public List<OrderDTO> list() {
        return new LinkedList<>();
//        List<Order> orders = orderRepository.findAll();
//        return orders.stream().map(order ->
//                        OrderDTO.builder()
//                                .amount(order.getAmount())
//                                .products(productsByIds(order))
//                                .build())
//                .collect(Collectors.toList());
    }

    @GetMapping("/open")
    public String open() {
        return "Open from order service";
    }

    @PostMapping("/test-rabbit")
    public String sendToShipping(@RequestBody Map<String, Object> map) {
        rabbitTemplate.convertAndSend(uzumExchange, routingKeyForShipping, map);
        return "Yubordik";
    }

    private List<ProductDTO> productsByIds(Order order) {
        List<String> productIds = order.getOrderItems().stream().map(OrderItem::getProductId).collect(Collectors.toList());
        return productClient.getProductsByIds(productIds);
    }

    @GetMapping("/battar")
    public String getBla() {
        System.out.println(environment.getProperty("app.default.page.size"));
        return environment.getProperty("app.default.page.size");
    }

    @PostMapping("/bratan-refresh-qil")
    public void githubdanKeldi(@RequestBody Object object) {
        System.out.println(object);
        new Thread(() ->
        {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type", "application/json");
            HttpEntity<?> entity = new HttpEntity<>(headers);
            ResponseEntity<Object> exchange = restTemplate.exchange("http://localhost:8956/actuator/refresh", HttpMethod.POST, entity, Object.class);
            System.out.println(exchange);
        }).start();
    }

}
