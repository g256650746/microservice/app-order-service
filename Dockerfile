FROM maven:3.9-eclipse-temurin-17-alpine AS MAVEN_BUILD

COPY pom.xml /build/

RUN mvn dependency:go-offline -B

COPY src /build/src/

WORKDIR /build/

RUN mvn package -DskipTests

FROM amazoncorretto:17-alpine3.16

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/*.jar /app/ketmon.jar

ENTRYPOINT ["java", "-jar", "ketmon.jar"]